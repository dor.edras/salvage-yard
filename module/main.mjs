/* Imports */
import { CONFIG } from './config.mjs';
import { SalvageYard } from './salvageyard.mjs';

Hooks.once('init', () => { SalvageYard.initialize(); });
Hooks.once('ready', () => { SalvageYard.ready(); });

/* Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(CONFIG.ID);
});