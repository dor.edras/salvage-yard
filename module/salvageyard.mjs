import { CONFIG } from "./config.mjs";
import { SalvageYardConfig } from "./forms/salvageyardConfig.mjs";

export class SalvageYard {
    static log(force, ...args) {
        const shouldLog = force || game.modules.get('_dev-mode')?.api?.getPackageDebugValue(CONFIG.ID);

        if (shouldLog) {
            console.log(CONFIG.ID, '|', ...args);
        }
    }

    static initialize() {
        //Register settings
        SalvageYard.log(true, "Initializing Salvage Yard...");
        game.settings.register(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES, {
            config: false,
            default: CONFIG.DOCUMENT_TYPES,
            scope: 'world',
            type: Object
        });

        game.settings.registerMenu(CONFIG.ID, "ModuleConfiguration", {
            name: "SalvageYardConfiguration",
            label: game.i18n.localize("SALVAGEYARD.CONFIG.LABEL"),
            hint: game.i18n.localize("SALVAGEYARD.CONFIG.HINT"),
            icon: "fas fa-cog",
            type: SalvageYardConfig
        });

        //Get Settings
        const settings = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES);
        for (let key of Object.keys(CONFIG.DOCUMENT_TYPES)) {
            if (settings[key]) {
                CONFIG.DOCUMENT_TYPES[key].enabled = settings[key].enabled;
            }
        }

        //Attach On Delete Hooks
        const types = Object.keys(CONFIG.DOCUMENT_TYPES);
        for (let t of types) {
            if (CONFIG.DOCUMENT_TYPES[t].enabled) {
                Hooks.on(`preDelete${CONFIG.DOCUMENT_TYPES[t].name}`, (document, options, userId) => { SalvageYard.SalvageDocument(CONFIG.DOCUMENT_TYPES[t], document, options, userId); });
            }
        }

        Hooks.on('preDeleteFolder', (document, options, userId) => { SalvageYard.SalvageFolder(document, options, userId); });
    }

    static ready() {
        if (!game.user.isGM) { return; }

        //check if compendiums exist, create if necessary
        const types = Object.keys(CONFIG.DOCUMENT_TYPES);
        for (let t of types) {
            let lowerPackName = `${CONFIG.ID}_${CONFIG.DOCUMENT_TYPES[t].packName}`.toLowerCase();
            if (!game.packs.get(`world.${lowerPackName}`)) {
                CompendiumCollection.createCompendium({
                    "label": `${CONFIG.FRIENDLY_NAME} ${CONFIG.DOCUMENT_TYPES[t].packName}`,
                    "type": CONFIG.DOCUMENT_TYPES[t].name,
                    "name": lowerPackName,
                    "package": CONFIG.ID,
                    "path": "packs/" + lowerPackName + ".db",
                });
            }
        }
    }

    static async SalvageDocument(salvageType, document, options, userId) {
        SalvageYard.log(false, salvageType, document, options, userId);
        if (!salvageType.enabled) { return; }

        let lowerPackName = `world.${CONFIG.ID}_${salvageType.packName}`.toLowerCase();
        if (document.pack === lowerPackName) { return; } // We don't salvage from the salvage yard

        try {
            let pack = game.packs.get(lowerPackName);
            pack.importDocument(document, {keepId: true});
            SalvageYard.log(true, `Salvage Yard just salvaged a document with id ${document.id}!`);
        } catch (e) {
            console.error("Error attempting to salvage a document!", e);
            SalvageYard.log(true, "Salvage Yard Error Information", salvageType, document, options, userId);
        }
    }

    static async SalvageFolder(document, options, userId) {
        let salvageType = CONFIG.DOCUMENT_TYPES[document.type.toUpperCase()];
        if (!salvageType.enabled) { return; }

        SalvageYard.log(true, `Salvage Yard detected the deletion of folder ${document.name} with id ${document.id}, salvaging documents...`);

        if (document.children.length) {
            for (let child of document.children) {
                await SalvageYard.SalvageFolder(child, options, userId);
            }
        }

        let lowerPackName = `world.${CONFIG.ID}_${salvageType.packName}`.toLowerCase();
        try {
            let pack = game.packs.get(lowerPackName);
            for (let d of document.content) {
                pack.importDocument(d, {keepId: true});
                SalvageYard.log(true, `Salvage Yard just salvaged a document with id ${d.id}!`);
            }
        } catch (e) {
            console.error("Error attempting to salvage a document!", e);
            SalvageYard.log(true, "Salvage Yard Error Information", salvageType, document, options, userId);
        }
    }
}