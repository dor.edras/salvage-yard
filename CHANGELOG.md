# Salvage Yard
## v1.0.2 - 2022-03-31
 - Removed salvage-yard.lock from repo, which...was never supposed to be there.  Sorry folks.
 
## v1.0.1 - 2022-03-17
 - Fixed issue with documents within a folder not getting salvaged when that folder (or parent folders) were being deleted, due to Foundry workflow not firing the preDelete hooks on these documents.

## v1.0.0 - 2022-03-06
- First release of Salvage Yard!